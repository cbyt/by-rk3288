#ifndef _CBYT_GPIO_H_
#define _CBYT_GPIO_H_



/* gpio define */
enum {
CBYT_GPIO_PWR_HOLD = 0,
CBYT_GPIO_CPU_DET,
CBYT_GPIO_PANEL,
CBYT_GPIO_WORK,
CBYT_GPIO_POWER,

CBYT_GPIO_FAN_CTL,
CBYT_GPIO_OUT1,
CBYT_GPIO_OUT2,
//CBYT_GPIO_OUT3,
CBYT_GPIO_IN1,
CBYT_GPIO_IN2,
CBYT_GPIO_IN3,
CBYT_GPIO_MAX
};

#define CBYT_GPIO_HIGH			1
#define CBYT_GPIO_LOW			0

/* ioctl command */

#define CBYT_GPIO_CMD_SET_OUT_VALUE		0x01
#define CBYT_GPIO_CMD_GET_IN_VALUE		0x02


/* ioctl args */
typedef struct cbyt_gpio_ctrl {
	int	num;	
	int val;

} cbyt_gpio_ctrl;


#endif
