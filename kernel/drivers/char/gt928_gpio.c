/*#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <dt-bindings/pinctrl/rockchip-rk3288.h>

#define LED_GPIO1 258
#define LED_GPIO2 257
#define GPIO_LOW 0
#define GPIO_HIGH 1

static void mos_short(void)
{
	gpio_set_value(LED_GPIO1,0);
        mdelay(500);
        gpio_set_value(LED_GPIO1,1);
        mdelay(500);
        printk("ssssssssssssssssssssssssss\n");
}

static int __init testgpio_init(void)
{
       int i;
        for(i=0;i<10;i++)
	{
		mos_short();
	}
       return 0;
}

static void __exit testgpio_exit(void)
{

}

module_init(testgpio_init);
module_exit(testgpio_exit);


MODULE_AUTHOR("ceshi@163.com");
MODULE_DESCRIPTION("LED CESHI");
MODULE_LICENSE("GLP");*/

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h> 
#include <linux/delay.h>
#include <linux/gpio.h>
//#ifdef CONFIG_OF
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
//#endif

#define CONFIG_OF 1
#define GPIO_LOW 0
#define GPIO_HIGH 1
#define GPIO_MAX  2

static int gt928_gpio_probe(struct platform_device *pdev)
{
    int ret = -1,ret1=-1;
    int i;
    int gpio_int,flag1,flag2,gpio_reset;
	struct device_node *hello_node = pdev->dev.of_node;

	printk("%s-%d: enter\n",__FUNCTION__,__LINE__);

	gpio_int = of_get_named_gpio_flags(hello_node,"led-int", 0,&flag1);
        gpio_reset = of_get_named_gpio_flags(hello_node,"led-reset", 0,&flag2);
	if (!gpio_is_valid(gpio_int)){
		printk("hello: invalid gpio : %d\n",gpio_int);
		return -1;
	} 
        ret = gpio_request(gpio_int, "gpio int");
        if (ret != 0) {
		gpio_free(gpio_int);
		return -EIO;
	}
        if (!gpio_is_valid(gpio_reset)){
		printk("hello: invalid gpio : %d\n",gpio_reset);
		return -1;
	} 
        ret1 = gpio_request(gpio_reset, "gpio reset");
	

        if (ret1 != 0) {
		gpio_free(gpio_reset);
		return -EIO;
	}
        
	/*gpio_direction_output(gpio_int, GPIO_HIGH);
        gpio_direction_output(gpio_reset, GPIO_HIGH);

	for(i=0; i < 10; i++)
	{
                printk("gpio_int = %d\n",gpio_int);
                printk("gpio_reset = %d\n",gpio_reset);
		gpio_set_value(gpio_int,GPIO_LOW);
		mdelay(500);
		gpio_set_value(gpio_reset,GPIO_LOW);
		mdelay(500);
	}*/
    	
	printk("%s-%d: exit\n",__FUNCTION__,__LINE__);
	
	return 0;  //return Ok
}


static int gt928_gpio_remove(struct platform_device *pdev)
{ 
    return 0;
}
#ifdef CONFIG_OF
static const struct of_device_id of_gt928_gpio_match[] = {
	{ .compatible = "gt928,gpio-touch" },
	{ /* Sentinel */ }
};
#endif

static struct platform_driver gt928_gpio_driver = {
	.probe		= gt928_gpio_probe,
	.remove		= gt928_gpio_remove,
	.driver		= {
		.name	= "gt928_gpio",
		.owner	= THIS_MODULE,
#ifdef CONFIG_OF
		.of_match_table	= of_gt928_gpio_match,
#endif
	},

};

static int __init hello_init(void)
{
    printk(KERN_INFO "Enter %s\n", __FUNCTION__);
    return platform_driver_register(&gt928_gpio_driver);
    return 0;
}

static void __exit hello_exit(void)
{
	platform_driver_unregister(&gt928_gpio_driver);
    	printk("Exit Hello world\n");
}

subsys_initcall(hello_init);
module_exit(hello_exit);

MODULE_AUTHOR("jizhang <zhangji@cbyt.com>");
MODULE_DESCRIPTION("GT928 GPIO driver");
MODULE_LICENSE("GPL");







