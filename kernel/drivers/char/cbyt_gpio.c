#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/fcntl.h>
#include <linux/mm.h>
#include <linux/proc_fs.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <asm/system.h>
#include <linux/miscdevice.h>
#include <linux/delay.h>

#include <linux/proc_fs.h>
#include <linux/poll.h>

#include <asm/bitops.h>
#include <asm/uaccess.h>
#include <asm/irq.h>

#include <linux/moduleparam.h>
#include <linux/ioport.h>
#include <linux/interrupt.h>

#include <linux/wakelock.h>
#include <linux/slab.h>
#include <linux/of_gpio.h>
#include <linux/irqdomain.h>
#include <linux/delay.h>
//////////////////////////////////////////

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/gpio.h>
#include <linux/leds.h>
#include <linux/of_platform.h>
#include <linux/of_gpio.h>
#include <linux/slab.h>
#include <linux/workqueue.h>
#include <linux/module.h>
#include <linux/pinctrl/consumer.h>
#include <linux/err.h>
#include <linux/version.h>   
#include <linux/proc_fs.h>   
#include <linux/fb.h>
#include <linux/rk_fb.h>
#include <linux/display-sys.h>
#ifdef CONFIG_OF
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <dt-bindings/rkfb/rk_fb.h>
#endif

#include "cbyt_gpio.h"

#define DEV_NAME "cbyt_gpio"



struct cbyt_gpio_desc {
		int io_num;
		char dir;
		char val;
		char *com;
		char *name;
};


struct cbyt_gpio_info {
	struct platform_device	*pdev;
	struct cbyt_gpio_desc desc[CBYT_GPIO_MAX];
};

struct cbyt_gpio_info gpio_info = 
{
	.pdev = NULL,
	{
                { .dir = 1, .com = "gpio-pwr-hold", .name = "gpio_pwr_hold"},
		{ .dir = 1, .com = "gpio-cpu_det", .name = "gpio_cpu_det"},
                { .dir = 1, .com = "gpio-panel", .name = "gpio_panel"},
                { .dir = 1, .com = "gpio-work", .name = "gpio_work"},
                { .dir = 1, .com = "gpio-power", .name = "gpio_power"},

                { .dir = 1, .com = "gpio-fan_ctl", .name = "gpio_fan_ctl"},
		{ .dir = 1, .com = "gpio-1", .name = "gpio_1"},
		{ .dir = 1, .com = "gpio-2", .name = "gpio_2"},
		//{ .dir = 1, .com = "gpio-3", .name = "gpio_3"},

		{ .dir = 0, .com = "gpio-4", .name = "gpio_4"},
		{ .dir = 0, .com = "gpio-5", .name = "gpio_5"},
		{ .dir = 0, .com = "gpio-6", .name = "gpio_6"},
	}

};
int cbyt_gpio_pwr_ionum = -1;
int cbyt_gpio_work_ionum = -1;
int cbyt_gpio_pwr_set(int val)
{
	if( cbyt_gpio_pwr_ionum> 0) {
		gpio_direction_output(cbyt_gpio_pwr_ionum, val);
	}
	if( cbyt_gpio_work_ionum> 0) {
		gpio_direction_output(cbyt_gpio_work_ionum, val);
	}
return 0;	
}
EXPORT_SYMBOL(cbyt_gpio_pwr_set);

int cbyt_gpio_cpu_det_ionum = -1;
int cbyt_gpio_cpu_det_set(int val)
{
	if( cbyt_gpio_cpu_det_ionum> 0) {
		gpio_direction_output(cbyt_gpio_cpu_det_ionum, val);
	}
return 0;	
}
EXPORT_SYMBOL(cbyt_gpio_cpu_det_set);

int cbyt_gpio_pwr_hold_ionum = -1;
int cbyt_gpio_pwr_hold_set(int val)
{
	if( cbyt_gpio_pwr_hold_ionum> 0) {
		gpio_direction_output(cbyt_gpio_pwr_hold_ionum, val);
	}
return 0;	
}

EXPORT_SYMBOL(cbyt_gpio_pwr_hold_set);


static int cbyt_gpio_open(struct inode * inode, struct file * file)
{
	return 0 ;
}

static int cbyt_gpio_close(struct inode *inode ,struct file *file)
{
    	return 0;
}

static long cbyt_gpio_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	unsigned int __user *argp = (unsigned int __user *)arg;
	cbyt_gpio_ctrl temp;
        cbyt_gpio_ctrl *ctrl;

	int io_num;
	int io_val;

	int ret;

	if(argp != NULL) {
        if(copy_from_user(&temp,argp,sizeof(cbyt_gpio_ctrl))) {   
    	    return -EFAULT;
   	 	}
	}
        ctrl = (cbyt_gpio_ctrl *)(&temp);
        if((ctrl->num >= CBYT_GPIO_MAX) || (ctrl->num < CBYT_GPIO_FAN_CTL)){
	        return -EFAULT;
        }
	io_num = gpio_info.desc[ctrl->num].io_num;
	io_val = ctrl->val;

	printk(KERN_INFO "cbyt_gpio_ioctl\n");
	switch(cmd) {
		case CBYT_GPIO_CMD_SET_OUT_VALUE:
		{
                        if((ctrl->num < CBYT_GPIO_FAN_CTL) || (ctrl->num > CBYT_GPIO_OUT2))
                                return -EFAULT;
			gpio_direction_output(io_num,io_val);
		}
		break;
		case CBYT_GPIO_CMD_GET_IN_VALUE:
		{
			if((ctrl->num < CBYT_GPIO_IN1) || (ctrl->num > CBYT_GPIO_IN3))
                                return -EFAULT;
                        ret = gpio_get_value(io_num);
	                temp.val = ret;
	                if(!copy_to_user(argp, &temp, sizeof(cbyt_gpio_ctrl))) {
		                printk(KERN_INFO "cbyt-gpio copy_to_user error\n");
	                }
	        }	
		break;

		default:
                        return -EFAULT;
		break;
	}

	return 0;
}


static struct file_operations cbyt_gpio_fops = {
	.owner		= THIS_MODULE,
	.unlocked_ioctl		= cbyt_gpio_ioctl,
	.open		= cbyt_gpio_open,
	.release	= cbyt_gpio_close
};

static struct miscdevice cbyt_gpio_dev = {
	MISC_DYNAMIC_MINOR,
	DEV_NAME,
	&cbyt_gpio_fops,
};


static int cbyt_gpio_probe(struct platform_device *pdev)
{
    	int ret = -1;
	int i;
    	int gpio, flag;

	struct device_node *gpio_node = pdev->dev.of_node;
	struct pinctrl_state	*gpio_state;

    	gpio_info.pdev = pdev;

	gpio_state = pinctrl_lookup_state(pdev->dev.pins->p, "gpio");

	if(IS_ERR(gpio_state)) {
		printk(KERN_INFO "no gpio pinctrl state\n");
	} else {
		printk(KERN_INFO "select gpio state\n");
		ret = pinctrl_select_state(pdev->dev.pins->p, gpio_state);
		printk(KERN_INFO "pinctrl_select_state ret = %d\n", ret);
	}

	for(i = 0; i < CBYT_GPIO_MAX; i++) {
                printk(KERN_INFO "gpio com:%s", gpio_info.desc[i].com);
		gpio = of_get_named_gpio_flags(gpio_node,gpio_info.desc[i].com, 0,&flag);
		if (!gpio_is_valid(gpio)){
			printk("invalid gpio: %d\n",gpio);
			return -1;
		}
 
		ret = gpio_request(gpio, gpio_info.desc[i].name);
		if (ret != 0) {
			gpio_free(gpio);
			ret = -EIO;
			goto failed_1;
		}

		gpio_info.desc[i].io_num = gpio;
		gpio_info.desc[i].val = (flag == OF_GPIO_ACTIVE_LOW)? 0:1;
                if((i == CBYT_GPIO_CPU_DET) || (i == CBYT_GPIO_PWR_HOLD)) //lixy: cpu_det, pwr_hold 
                        continue;
		if(gpio_info.desc[i].dir == 1)		
			gpio_direction_output(gpio_info.desc[i].io_num, gpio_info.desc[i].val);
		else
			gpio_direction_input(gpio_info.desc[i].io_num);

	}

	/* registe misc driver */
	ret = misc_register(&cbyt_gpio_dev);
	if(ret) {
		printk(KERN_INFO "could not register cbyt_gpio device");
		return -1;
	}

	/* cpu_det */
	cbyt_gpio_cpu_det_ionum = gpio_info.desc[CBYT_GPIO_CPU_DET].io_num;
	gpio_direction_output(cbyt_gpio_cpu_det_ionum, 1);

        mdelay(10);
        cbyt_gpio_pwr_hold_ionum = gpio_info.desc[CBYT_GPIO_PWR_HOLD].io_num;
	gpio_direction_output(cbyt_gpio_pwr_hold_ionum, 1);

        cbyt_gpio_pwr_ionum = gpio_info.desc[CBYT_GPIO_POWER].io_num;
        cbyt_gpio_work_ionum = gpio_info.desc[CBYT_GPIO_WORK].io_num;

	printk(KERN_INFO "cbyt_gpio_probe success\n");

	return 0;

failed_1:
	return ret;
}


static int cbyt_gpio_remove(struct platform_device *pdev)
{
	
	int i;

	printk(KERN_INFO "cbyt_gpio_remove\n");

	for(i = 0; i < CBYT_GPIO_MAX; i++) {
		gpio_free(gpio_info.desc[i].io_num);
	}

 	misc_deregister(&cbyt_gpio_dev);
    return 0;
}

#ifdef CONFIG_OF
static const struct of_device_id of_cbyt_gpio_match[] = {
	{ .compatible = "cbyt,gpio" },
	{ /* Sentinel */ }
};
#endif

static struct platform_driver cbyt_gpio_driver = {
	.probe		= cbyt_gpio_probe,
	.remove		= cbyt_gpio_remove,
	.driver		= {
		.name	= "cbyt-gpio",
		.owner	= THIS_MODULE,
#ifdef CONFIG_OF
		.of_match_table	= of_cbyt_gpio_match,
#endif
	},

};


static int __init cbyt_gpio_init(void)
{
    printk(KERN_INFO "cbyt_gpio_init\n");
    return platform_driver_register(&cbyt_gpio_driver);
}

static void __exit cbyt_gpio_exit(void)
{
	platform_driver_unregister(&cbyt_gpio_driver);
    printk(KERN_INFO "cbyt_gpio_exit\n");
}

module_init(cbyt_gpio_init);
module_exit(cbyt_gpio_exit);

MODULE_AUTHOR("lixy <lixy@cbyt.com.cn>");
MODULE_DESCRIPTION("CBYT GPIO driver");
MODULE_LICENSE("GPL");
