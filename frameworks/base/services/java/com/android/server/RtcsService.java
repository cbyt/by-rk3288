package com.android.server;

import android.content.Context;
import android.os.IRtcsService;
import android.util.Slog;

public class RtcsService extends IRtcsService.Stub{
	private static final String TAG = "RtcsService";
	
	private int mPtr = 0;
        

	RtcsService() {
		mPtr = init_native();
		
		if(mPtr == 0) {
			Slog.e(TAG, "Failed to initialize rtcs service.");
		}
	}

	public int getTime(int[] kptr) {
		if(mPtr == 0) {
			Slog.e(TAG, "Rtcs service is not initialized.");
			return 0;
		}

		//mPtr = getVal_native(kptr);
		//return mptr[1];
                return getTime_native(kptr);
	}

	public int setTime(int[] kptr)
	{
		if(mPtr == 0) {
			Slog.e(TAG, "Flexcan service is not initialized.");
			return 0;
		}
		return setTime_native(kptr);
	}
	
	public int setWaketime(int[] kptr)
	{
		if(mPtr == 0) {
			Slog.e(TAG, "Flexcan service is not initialized.");
			return 0;
		}
		return setWaketime_native(kptr);
	}
	private static native int init_native();
	private static native int getTime_native(int[] ptr);
	private static native int setTime_native(int[] ptr);
	private static native int setWaketime_native(int[] ptr);
}
