#define LOG_TAG "RtctServiceJNI"

#include "jni.h"
#include "JNIHelp.h"
#include "android_runtime/AndroidRuntime.h"

#include <utils/misc.h>
#include <utils/Log.h>
#include <hardware/hardware.h>
#include <hardware/rtcs.h>

#include <stdio.h>


namespace android {
	static struct rtcs_device_t* device = NULL;
	static inline int rtcs_device_open(const hw_module_t* module,struct rtcs_device_t** device)
	{
		return module->methods->open(module,RTCS_HARDWARE_DEVICE_ID,(struct hw_device_t**)device);
	}

	static jint rtcs_init(JNIEnv* env, jclass clazz) {
		rtcs_module_t* module;
		//rtcs_device_t* device;
                
		
		//ALOGI("Initializing HAL stub rtcs......");

		if(hw_get_module(RTCS_HARDWARE_MODULE_ID, (const struct hw_module_t**)&module) == 0) {
			//ALOGI("Device temperaturek found.");
			if(rtcs_device_open(&(module->common), &device) == 0) {
				//ALOGI("Device RTCS is open.");
				return (jint)device;
				//return 0;
			}

			ALOGE("Failed to open device temperaturek.");
			return 0;
		}

		ALOGE("Failed to get HAL stub rtcs.");

		return 0;		
	}

        static jint rtcs_getTime(JNIEnv* env, jobject clazz, jintArray ptr)
	{
		//ALOGI("rtcs_getTime is goto here......");
		//rtcs_device_t* device = (rtcs_device_t*)ptr;
               // rtcs_device_t* device = NULL;
		/*if(!device) {
			ALOGE("Device rtcs is not open.");
			return 0;
		}*/
		jint* p = env->GetIntArrayElements(ptr,0);
		int ret;
		int t[6] ;
		//ALOGI("t is goto here1......");
		ret = device->rtcs_get_time(device, t);
		//ALOGI("t is goto here2......");
                //ALOGI("t date is %d,%d\n",t[0],t[1]);
		for(int i=0;i<6;i++)
		{
			//ALOGI("t date is %d\n",t[i]);
			*(p+i) = t[i];
		}
		env->ReleaseIntArrayElements(ptr,p,0);
		return 0;
		
	}
        
        static jint rtcs_setTime(JNIEnv* env, jobject clazz, jintArray ptr)
	{
		/*rtcs_device_t* device = (rtcs_device_t*)ptr;
		if(!device) {
			//ALOGE("Device freg is not open.");
			return 0;
		}*/
	
		jint* p = env->GetIntArrayElements(ptr,0);
		device->rtcs_set_time(device, p);
		env->ReleaseIntArrayElements(ptr,p,0);
		return 0;
	}

        static jint rtcs_setWaketime(JNIEnv* env, jobject clazz, jintArray ptr)
	{
		/*rtcs_device_t* device = (rtcs_device_t*)ptr;
		if(!device) {
			//ALOGE("Device freg is not open.");
			return 0;
		}*/
	
		jint* p = env->GetIntArrayElements(ptr,0);
		device->rtcs_set_waketime(device, p);
		env->ReleaseIntArrayElements(ptr,p,0);
		return 0;
	}

	static const JNINativeMethod method_table[] = {
		{"init_native", "()I", (void*)rtcs_init},
		{"getTime_native", "([I)I", (void*)rtcs_getTime},
		{"setTime_native", "([I)I", (void*)rtcs_setTime},
		{"setWaketime_native", "([I)I", (void*)rtcs_setWaketime},
	};

	int register_android_server_RtcsService(JNIEnv *env) {
    		return jniRegisterNativeMethods(env, "com/android/server/RtcsService", method_table, NELEM(method_table));
	}


	
}
