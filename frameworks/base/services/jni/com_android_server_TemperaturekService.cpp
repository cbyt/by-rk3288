#define LOG_TAG "TemperaturekServiceJNI"

#include "jni.h"
#include "JNIHelp.h"
#include "android_runtime/AndroidRuntime.h"

#include <utils/misc.h>
#include <utils/Log.h>
#include <hardware/hardware.h>
#include <hardware/temperaturek.h>

#include <stdio.h>

namespace android
{
	/*static void freg_setVal(JNIEnv* env, jobject clazz, jint ptr, jint value) {
		freg_device_t* device = (freg_device_t*)ptr;
		if(!device) {
			LOGE("Device freg is not open.");
			return;
		}	
	
		int val = value;

		LOGI("Set value %d to device freg.", val);
		
		device->set_val(device, val);
	}*/

	static jint temperaturek_getVal(JNIEnv* env, jobject clazz, jint ptr) {
		temperaturek_device_t* device = (temperaturek_device_t*)ptr;
		if(!device) {
			//LOGE("Device freg is not open.");
			return 0;
		}

		char t[128];
		int k=0;
                int i=0;
		int l=0;

		device->temperaturek_get_val(device, t);
		//ALOGI("Get value %s from device freg.", t);
                for(i=0;i<128;i++)
		{
	    		if((t[i]=='t') && (t[i+1]=='=') && (t[i+2]!='-'))
	    		{

			if(t[i+7]-48 <0 )
		   		k = (t[i+2]-48)*10000+(t[i+3]-48)*1000+(t[i+4]-48)*100+(t[i+5]-48)*10+t[i+6]-48;
			else
		   		k = (t[i+2]-48)*100000+(t[i+3]-48)*10000+(t[i+4]-48)*1000+(t[i+5]-48)*100+(t[i+6]-48)*10+(t[i+7]-48);
	    		}

	    		if((t[i]=='t') && (t[i+1]=='=') && (t[i+2]=='-'))
	    		{
		 		if(t[i+8]-48 <0 )
		    			k = (-1)*((t[i+3]-48)*10000+(t[i+4]-48)*1000+(t[i+5]-48)*100+(t[i+6]-48)*10+(t[i+7]-48));
		 		else
		    			k = (-1)*((t[i+3]-48)*100000+(t[i+4]-48)*10000+(t[i+5]-48)*1000+(t[i+6]-48)*100+(t[i+7]-48)*10+(t[i+8]-48));
	     		}
	  	}
		l = k/1000;
		//ALOGI("Get value %d from device freg.", l);
	
		return l;
	}

	static inline int temperaturek_device_open(const hw_module_t* module, struct temperaturek_device_t** device) {
		return module->methods->open(module, TEMPERATUREK_HARDWARE_DEVICE_ID, (struct hw_device_t**)device);
	}
	
	static jint temperaturek_init(JNIEnv* env, jclass clazz) {
		temperaturek_module_t* module;
		temperaturek_device_t* device;
                
		
		//LOGI("Initializing HAL stub freg......");

		if(hw_get_module(TEMPERATUREK_HARDWARE_MODULE_ID, (const struct hw_module_t**)&module) == 0) {
			//LOGI("Device temperaturek found.");
			if(temperaturek_device_open(&(module->common), &device) == 0) {
				//LOGI("Device temperaturek is open.");
				return (jint)device;
			}

			//LOGE("Failed to open device temperaturek.");
			return 0;
		}

		//LOGE("Failed to get HAL stub freg.");

		return 0;		
	}

	static const JNINativeMethod method_table[] = {
		{"init_native", "()I", (void*)temperaturek_init},
		{"getVal_native", "(I)I", (void*)temperaturek_getVal},
	};

	int register_android_server_TemperaturekService(JNIEnv *env) {
    		return jniRegisterNativeMethods(env, "com/android/server/TemperaturekService", method_table, NELEM(method_table));
	}
};

