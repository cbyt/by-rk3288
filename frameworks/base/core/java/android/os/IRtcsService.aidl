package android.os;

interface IRtcsService {
	int getTime(inout int[] val);
	int setTime(in int[] val);
        int setWaketime(in int[] val);
}

