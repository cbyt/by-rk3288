$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base.mk)
include device/rockchip/rk3288/BoardConfig.mk
$(call inherit-product, device/rockchip/rksdk/device.mk)
PRODUCT_BRAND := rockchip
PRODUCT_DEVICE := rk3288
PRODUCT_NAME := rk3288
PRODUCT_MANUFACTURER := rockchip

# Get the long list of APNs
PRODUCT_COPY_FILES += device/rockchip/common/phone/etc/apns-full-conf.xml:system/etc/apns-conf.xml
PRODUCT_COPY_FILES += device/rockchip/common/phone/etc/spn-conf.xml:system/etc/spn-conf.xml
# sulq add bootanimation
PRODUCT_COPY_FILES += device/rockchip/rk3288/bootanimation.zip:system/media/bootanimation.zip

PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp/ESBrowser_cbyt.apk:system/app/ESBrowser_cbyt.apk
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp/iFly_TTS_cbyt.apk:system/app/iFly_TTS_cbyt.apk
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp/iFly_TTS_Resource-v1.0_cbyt.apk:system/app/iFly_TTS_Resource-v1.0_cbyt.apk
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp/LogcatExtreme_cbyt.apk:system/app/LogcatExtreme_cbyt.apk
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp/ludashi_cbyt.apk:system/app/ludashi_cbyt.apk
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp/openvpnkhd_v1.1.11_cbyt.apk:system/app/openvpnkhd_v1.1.11_cbyt.apk
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp/Shell_cmd_cbyt.apk:system/app/Shell_cmd_cbyt.apk
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp/SQLite_cbyt.apk:system/app/SQLite_cbyt.apk
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp/sshdroid_cbyt.apk:system/app/sshdroid_cbyt.apk
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp/txtEdit_cbyt.apk:system/app/txtEdit_cbyt.apk
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp/Xplorer_cbyt.apk:system/app/Xplorer_cbyt.apk

PRODUCT_COPY_FILES += device/rockchip/rk3288/busybox_install/busybox.tar.gz:system/xbin/busybox.tar.gz

PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libCharsetDetector.so:system/lib/libCharsetDetector.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libcrypt.so:system/lib/libcrypt.so:
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libdaemonproxy.so:system/lib/libdaemonproxy.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/liberase.so:system/lib/liberase.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libgrep.so:system/lib/libgrep.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libhighlight.so:system/lib/libhighlight.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libldsBench_1.so:system/lib/libldsBench_1.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libLDSBenchmarkMobile.so:system/lib/libLDSBenchmarkMobile.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libldsBench_v7_1.so:system/lib/libldsBench_v7_1.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libldsdaemon.so:system/lib/libldsdaemon.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libldsdaemon_2.so:system/lib/libldsdaemon_2.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libldsdaemon_5.so:system/lib/libldsdaemon_5.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libmyaes.so:system/lib/libmyaes.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libovpncli.so:system/lib/libovpncli.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libsapi_so_1.so:system/lib/libsapi_so_1.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libSwirlEngine.so:system/lib/libSwirlEngine.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libterm-androidterm4.so:system/lib/libterm-androidterm4.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libttsaisound.so:system/lib/libttsaisound.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libupdate-1.0.0.so:system/lib/libupdate-1.0.0.so
PRODUCT_COPY_FILES += device/rockchip/rk3288/cbytapp_lib/libwrapper.so:system/lib/libwrapper.so




PRODUCT_COPY_FILES += device/rockchip/rk3288/ssh_config/gen.pub:system/gen.pub
PRODUCT_COPY_FILES += device/rockchip/rk3288/androidvncserver:system/bin/androidvncserver


PRODUCT_PROPERTY_OVERRIDES += \
    ro.product.version = 1.0.0 \
    ro.product.ota.host = www.rockchip.com:2300



