#include <hardware/hardware.h>
#include <hardware/rtcs.h>

#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <linux/rtc.h>
#include <sys/ioctl.h>
#include <sys/time.h>'
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
//#include <linux/andorid_alarm.h>

#include <cutils/log.h>
#include <cutils/atomic.h>


#define LOG_TAG "rtcsHALStub"
#define MODULE_NAME "rtcs"
#define MODULE_AUTHOR	"CBYT_ZH"
#define DEVICE_NAME "/dev/rtc0"


static int rtcs_device_open(const struct hw_module_t* module, const char* id, struct hw_device_t** device);
static int rtcs_device_close(struct hw_device_t* device);
static int rtcs_get_time(struct rtcs_device_t* dev, int get_time[6]);
static int rtcs_set_time(struct rtcs_device_t* dev,int set_time[6]);
static int rtcs_set_waketime(struct rtcs_device_t* dev,int set_time[6]);



struct timeval *tv;
struct rtc_time rtc_tm;
struct tm tm;

static struct hw_module_methods_t rtcs_module_methods = {
	open: rtcs_device_open
};

struct rtcs_module_t HAL_MODULE_INFO_SYM = {
	common: {
		tag: HARDWARE_MODULE_TAG,	
		version_major: 1,
		version_minor: 0,
		id: RTCS_HARDWARE_MODULE_ID,
		name: MODULE_NAME,
		author: MODULE_AUTHOR,
		methods: &rtcs_module_methods,
	}
};

static int rtcs_device_open(const struct hw_module_t* module, const char* id, struct hw_device_t** device){

	struct rtcs_device_t* dev;
	dev = (struct rtcs_device_t*)malloc(sizeof(struct rtcs_device_t));
	if(!dev) {
		//ALOGE("Failed to alloc space for rtcs_device_t.");
		return -1;	
	}
       // ALOGI("Open device file /dev/rtc0 successfullyss.");
	memset(dev, 0, sizeof(struct rtcs_device_t));

	dev->common.tag = HARDWARE_DEVICE_TAG;
	dev->common.version = 0;
	dev->common.module = (hw_module_t*)module;
	dev->common.close = rtcs_device_close;
	dev->rtcs_get_time = rtcs_get_time;
	dev->rtcs_set_time = rtcs_set_time;
	dev->rtcs_set_waketime = rtcs_set_waketime;

	/*if((dev->fd = open(DEVICE_NAME, O_RDWR)) == -1) {
		ALOGE("Failed to open device file /dev/rtc0 -- %s.", strerror(errno));
		free(dev);
		return -1;
	}*/
	*device=&(dev->common);
	ALOGI("Open device file /dev/rtc0 successfully.");
	return 0;
}

static int rtcs_device_close(struct hw_device_t* device){
	struct rtcs_device_t* rtcs_device = (struct rtcs_device_t*)device;
	if(rtcs_device) {
		//close(rtcs_device->fd);
		free(rtcs_device);
	}
	return 0;
}

static int rtcs_get_time(struct rtcs_device_t* rtcs_device, int get_time[6]){
	ALOGI("t is  successfully1.");
	/*if(!rtcs_device) {
		ALOGE("Null dev pointer.");
		return -1;
	}*/

	/*if(!get_time) {
		//LOGE("Null val pointer.");
		return -1;
	}*/
	int res=-1,retval=-1;
	ALOGI("t1 is  successfully2.");
	if((rtcs_device->fd = open(DEVICE_NAME, O_RDWR)) == -1) {
		ALOGE("Failed to open device file1 /dev/rtc0 -- %s.", strerror(errno));
		free(rtcs_device);
		return -1;
	}
	retval = ioctl(rtcs_device->fd,RTC_RD_TIME,&rtc_tm);
        if(retval == -1)
	{
		ALOGE("Failed to open device file2 /dev/rtc0 -- %s.", strerror(errno));
                return -1;
	}
		//ALOGI("IOCTL RET_RD_TIME successfully.");
        //ALOGI("DDDDDDDDDDDDDDDDDDDDDDDD -- %d,%d,%d,%d,%d,%d\n",rtc_tm.tm_year+1900,rtc_tm.tm_mon+1,rtc_tm.tm_mday,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec);
        get_time[0] = rtc_tm.tm_year + 1900;
        get_time[1] = rtc_tm.tm_mon + 1;
	get_time[2] = rtc_tm.tm_mday;
        get_time[3] = rtc_tm.tm_hour;
        get_time[4] = rtc_tm.tm_min;	
        get_time[5] = rtc_tm.tm_sec; 
        close(rtcs_device->fd);
        return 0;
}

static int rtcs_set_time(struct rtcs_device_t* rtcs_device,int set_time[6])
{
	/*if(!rtcs_device) {
		ALOGE("Null dev pointer.");
		return -1;
	}*/
	
        if((rtcs_device->fd = open(DEVICE_NAME, O_RDWR)) == -1) {
		ALOGE("Failed to open device file1 /dev/rtc0 -- %s.", strerror(errno));
		free(rtcs_device);
		return -1;
	}
	int res=-1,retval=-1;
        rtc_tm.tm_year = set_time[0] - 1900;
        rtc_tm.tm_mon = set_time[1] - 1;
	rtc_tm.tm_mday = set_time[2];
        rtc_tm.tm_hour = set_time[3];
        rtc_tm.tm_min = set_time[4];
        rtc_tm.tm_sec = set_time[5];
	retval = ioctl(rtcs_device->fd,RTC_SET_TIME,&rtc_tm);

        if(retval == -1)
	{
		ALOGE("Failed to open device file /dev/rtc0 -- %s.", strerror(errno));
                return -1;
	}
	close(rtcs_device->fd);
        return 0;
}

static int rtcs_set_waketime(struct rtcs_device_t* rtcs_device,int set_time[6])
{
	/*if(!rtcs_device) {
		ALOGE("Null dev pointer.");
		return -1;
	}*/
	if((rtcs_device->fd = open(DEVICE_NAME, O_RDWR)) == -1) {
		ALOGE("Failed to open device file1 /dev/rtc0 -- %s.", strerror(errno));
		free(rtcs_device);
		return -1;
	}
	int res=-1,retval=-1;
        rtc_tm.tm_year = set_time[0] - 1900;
        rtc_tm.tm_mon = set_time[1] - 1;
	rtc_tm.tm_mday = set_time[2];
        rtc_tm.tm_hour = set_time[3];
        rtc_tm.tm_min = set_time[4];
        rtc_tm.tm_sec = set_time[5];
	retval = ioctl(rtcs_device->fd,RTC_ALM_SET,&rtc_tm);
        if(retval == -1)
	{
		ALOGE("Failed to open device file /dev/rtc0 -- %s.", strerror(errno));
                return -1;
	}
	close(rtcs_device->fd);
        return 0;
}
