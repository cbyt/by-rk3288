#include <hardware/hardware.h>
#include <hardware/temperaturek.h>

#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include <cutils/log.h>
#include <cutils/atomic.h>


#define LOG_TAG "temperaturekHALStub"
#define MODULE_NAME "temperaturek"
#define MODULE_AUTHOR	"CBYT_Z"
//#define DEVICE_NAME "/sys/bus/w1/devices/28-000006733dbc/w1_slave"



static int temperaturek_device_open(const struct hw_module_t* module, const char* id, struct hw_device_t** device);
static int temperaturek_device_close(struct hw_device_t* device);
static int temperaturek_get_val(struct temperaturek_device_t* dev, char* val);


static struct hw_module_methods_t temperaturek_module_methods = {
	open: temperaturek_device_open
};

struct temperaturek_module_t HAL_MODULE_INFO_SYM = {
	common: {
		tag: HARDWARE_MODULE_TAG,	
		version_major: 1,
		version_minor: 0,
		id: TEMPERATUREK_HARDWARE_MODULE_ID,
		name: MODULE_NAME,
		author: MODULE_AUTHOR,
		methods: &temperaturek_module_methods,
	}
};

static int temperaturek_device_open(const struct hw_module_t* module, const char* id, struct hw_device_t** device){

	struct temperaturek_device_t* dev;
	dev = (struct temperaturek_device_t*)malloc(sizeof(struct temperaturek_device_t));
	if(!dev) {
		//LOGE("Failed to alloc space for temperaturek_device_t.");
		return -1;	
	}

	memset(dev, 0, sizeof(struct temperaturek_device_t));

	dev->common.tag = HARDWARE_DEVICE_TAG;
	dev->common.version = 0;
	dev->common.module = (hw_module_t*)module;
	dev->common.close = temperaturek_device_close;
	dev->temperaturek_get_val = temperaturek_get_val;

	/*if((dev->fd = open(DEVICE_NAME, O_RDONLY)) == -1) {
		//LOGE("Failed to open device file /sys/bus/w1/devices/28-000006733dbc/w1_slave -- %s.", strerror(errno));
		free(dev);
		return -1;
	}*/
	*device=&(dev->common);
	//LOGI("Open device file /sys/bus/w1/devices/28-000006733dbc/w1_slave successfully.");
	return 0;
}

static int temperaturek_device_close(struct hw_device_t* device){
	struct temperaturek_device_t* temperaturek_device = (struct temperaturek_device_t*)device;
	if(temperaturek_device) {
		//close(temperaturek_device->fd);
		free(temperaturek_device);
	}
	return 0;
}

static int temperaturek_get_val(struct temperaturek_device_t* dev, char* val) {
        int i = 0;
        int j = 0;
        char device_name[100] = "/sys/bus/w1/devices/";
	char *temp = NULL;
	char *last_string = "/w1_slave"  ;

	if(!dev) {
		//LOGE("Null dev pointer.");
		return -1;
	}
	
	if(!val) {
		//LOGE("Null val pointer.");
		
		return -1;
	}
        //int k =0;
        if((dev->pd=opendir("/sys/bus/w1/devices/")) == NULL)
	{	
		//ALOGE("this is 1 NULL？.");
		return -1;
	}
	
	else
        {
                while(dev->ptr=readdir(dev->pd))
		{
			if(i==2)
			{
				//ALOGI("this is 2");
				temp = dev->ptr->d_name;
                                if(strcmp(temp,"w1_bus_master1")==0)
				{
					j=1;
				}
                                //ALOGI("this is 2 name is  %s.", temp);
				
                                //printf("%s\n",dev->ptr->d_name);
			}
			
			if( (j==1) || (i==3) )
			{
				temp = dev->ptr->d_name;
				 //ALOGI("this is 21 name is  %s.", temp);
				 j = 0;
			}
			//printf("%s\n",ptr->d_name);
                        //ALOGI("this is 3 name is  %s.", dev->ptr->d_name);
			i++;
			//ALOGI("this is 3 name is  %d", i);
                        //printf("i = %d\n",i);
		}
                //i=0;
		closedir(dev->pd);
		if(i>3){
			//ALOGI("this is 5");
			
			strcat(device_name,temp);
                        strcat(device_name,last_string);

			// ALOGI("this is 6 name is  %s.", device_name);
                       //printf("okokokokokokokokokokokoko\n");
			//temp = ptr->d_name;
			//printf("%s\n",ptr->d_name);
		}
                else
		{
			return -1;
		}
		//i=0;
               
	}

	if((dev->fd = open(device_name, O_RDONLY)) == -1) {
		//LOGE("Failed to open device file /sys/bus/w1/devices/28-000006733dbc/w1_slave -- %s.", strerror(errno));
		//free(dev);
		return -1;
	}
	read(dev->fd, val, 128);

	/*for(i=0;i<strlen(val);i++)
	{
	    if((t[i]=='t') && (t[i+1]=='=') && (t[i+2]!='-'))
	    {

		if(t[i+7]-48 <0 )
		   k = (t[i+2]-48)*10000+(t[i+3]-48)*1000+(t[i+4]-48)*100+(t[i+5]-48)*10+t[i+6]-48;
		else
		   k = (t[i+2]-48)*100000+(t[i+3]-48)*10000+(t[i+4]-48)*1000+(t[i+5]-48)*100+(t[i+6]-48)*10+(t[i+7]-48);
	    }

	    if((t[i]=='t') && (t[i+1]=='=') && (t[i+2]=='-'))
	    {
		 if(t[i+8]-48 <0 )
		    k = (-1)*((t[i+3]-48)*10000+(t[i+4]-48)*1000+(t[i+5]-48)*100+(t[i+6]-48)*10+(t[i+7]-48));
		 else
		    k = (-1)*((t[i+3]-48)*100000+(t[i+4]-48)*10000+(t[i+5]-48)*1000+(t[i+6]-48)*100+(t[i+7]-48)*10+(t[i+8]-48));
	     }
	  }*/
         //val = k;
	close(dev->fd);
	//ALOGI("Get value SSSSSSSSSSSS %d from device file /dev/freg.", strlen(val));
	//ALOGI("Get value kkkkkkkkkkkk %s from device file /dev/freg.", val);

	return 0;
}
