#ifndef ANDROID_INCLUDE_HARDWARE_TEMPERATUREK_H
#define ANDROID_INCLUDE_HARDWARE_TEMPERATUREK_H
//head file
#include <hardware/hardware.h>

#include <stdint.h>
#include <sys/cdefs.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>

__BEGIN_DECLS

//define module id
#define TEMPERATUREK_HARDWARE_MODULE_ID	"temperaturek"

/**
 * The id of this device
 */
#define TEMPERATUREK_HARDWARE_DEVICE_ID "temperaturek"


//hardware module struct
struct temperaturek_module_t
{
	struct hw_module_t common;
};

//hardware interface struct
struct temperaturek_device_t
{
	struct hw_device_t common;
	int fd;
        DIR *pd;
        struct dirent *ptr;
	int (*temperaturek_get_val)(struct temperaturek_device_t* temperaturek_device,char *val);
};
__END_DECLS

#endif
