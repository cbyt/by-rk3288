#ifndef ANDROID_INCLUDE_HARDWARE_RTCS_H
#define ANDROID_INCLUDE_HARDWARE_RTCS_H
//head file
#include <hardware/hardware.h>

#include <stdint.h>
#include <sys/cdefs.h>
#include <sys/types.h>

__BEGIN_DECLS

//define module id
#define RTCS_HARDWARE_MODULE_ID	"rtcs"

/**
 * The id of this device
 */
#define RTCS_HARDWARE_DEVICE_ID "rtcs"


//hardware module struct
struct rtcs_module_t
{
	struct hw_module_t common;
};

//hardware interface struct
struct rtcs_device_t
{
	struct hw_device_t common;
	int fd;
	int (*rtcs_get_time)(struct rtcs_device_t* rtcs_device,int get_time[6]);
	int (*rtcs_set_time)(struct rtcs_device_t* rtcs_device,int set_time[6]);
	int (*rtcs_set_waketime)(struct rtcs_device_t* rtcs_device,int set_time[6]);
};


__END_DECLS


#endif
